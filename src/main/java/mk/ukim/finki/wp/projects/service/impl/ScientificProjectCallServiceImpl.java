package mk.ukim.finki.wp.projects.service.impl;

import mk.ukim.finki.wp.projects.model.ScientificProjectCall;
import mk.ukim.finki.wp.projects.model.GrantHolder;
import mk.ukim.finki.wp.projects.model.dto.AddScientificProjectCallDto;
import mk.ukim.finki.wp.projects.model.dto.GrantHolderDto;
import mk.ukim.finki.wp.projects.model.dto.ScientificProjectCallDto;
import mk.ukim.finki.wp.projects.model.exceptions.ScientificProjectCallNotFoundException;
import mk.ukim.finki.wp.projects.model.exceptions.GrantHolderNotFound;
import mk.ukim.finki.wp.projects.repository.ScientificProjectCallRepository;
import mk.ukim.finki.wp.projects.service.ScientificProjectCallService;
import mk.ukim.finki.wp.projects.service.GrantHolderService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class ScientificProjectCallServiceImpl implements ScientificProjectCallService {


    private final ScientificProjectCallRepository scientificProjectCallRepository;
    private final GrantHolderService grantHolderService;

    public ScientificProjectCallServiceImpl(ScientificProjectCallRepository scientificProjectCallRepository, GrantHolderService grantHolderService) {
        this.scientificProjectCallRepository = scientificProjectCallRepository;
        this.grantHolderService = grantHolderService;
    }


    @Override
    public List<ScientificProjectCall> findAll() {
        return this.scientificProjectCallRepository.findAll();
    }

    @Override
    public Optional<ScientificProjectCall> findById(Long id) {
        return this.scientificProjectCallRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        ScientificProjectCall scientificProjectCall = this.findById(id).orElseThrow(ScientificProjectCallNotFoundException::new);
        this.scientificProjectCallRepository.delete(scientificProjectCall);
    }

    @Override
    public Optional<ScientificProjectCall> addCall(AddScientificProjectCallDto scientificProjectCallDto) {
        GrantHolderDto grantHolderDto=new GrantHolderDto(scientificProjectCallDto.getGrantHolderName(),scientificProjectCallDto.getGrantHolderDescription());
        GrantHolder grantHolder=this.grantHolderService.addInstitution(grantHolderDto).orElseThrow(GrantHolderNotFound::new);

        ScientificProjectCall scientificProjectCall = new ScientificProjectCall();
//        scientificProjectCall.setAcronym(scientificProjectCallDto.getAcronym());
//        scientificProjectCall.setTypeStatus(scientificProjectCallDto.getTypeStatus());
//        scientificProjectCall.setGrantHolder(grantHolder);
//        scientificProjectCall.setEndDate(scientificProjectCallDto.getEndDate());
//        scientificProjectCall.setTypeStatus(scientificProjectCallDto.getTypeStatus());
        return Optional.of(this.scientificProjectCallRepository.save(scientificProjectCall));

    }

    @Override
    public Optional<ScientificProjectCall> editCall(Long id, ScientificProjectCallDto scientificProjectCallDto) {
        ScientificProjectCall scientificProjectCall = this.findById(id).orElseThrow(ScientificProjectCallNotFoundException::new);
        GrantHolder grantHolder = this.grantHolderService.findById(scientificProjectCallDto.getInstitutionId()).orElseThrow(GrantHolderNotFound::new);


        scientificProjectCall.setName(scientificProjectCallDto.getName());
//        scientificProjectCall.setAcronym(scientificProjectCallDto.getAcronym());
//        scientificProjectCall.setTypeStatus(scientificProjectCallDto.getTypeStatus());
//        scientificProjectCall.setGrantHolder(grantHolder);
//        scientificProjectCall.setEndDate(scientificProjectCallDto.getEndDate());
//        scientificProjectCall.setTypeStatus(scientificProjectCallDto.getTypeStatus());


        return Optional.of(this.scientificProjectCallRepository.save(scientificProjectCall));
    }

    @Override
    public Page<ScientificProjectCall> findAllByPagination(Pageable pageable) {
        return this.scientificProjectCallRepository.findAll(pageable);
    }
}
