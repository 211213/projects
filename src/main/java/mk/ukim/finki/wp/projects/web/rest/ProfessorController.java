package mk.ukim.finki.wp.projects.web.rest;

import mk.ukim.finki.wp.projects.model.Professor;
import mk.ukim.finki.wp.projects.model.dto.ProfessorDto;
import mk.ukim.finki.wp.projects.service.ProfessorService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/projects/professors")
@CrossOrigin(origins = {"http://localhost:3000","*"})
public class ProfessorController {
    private final ProfessorService professorService;

    public ProfessorController(ProfessorService professorService) {
        this.professorService = professorService;
    }

    @GetMapping("/all")
    public List<Professor> findAllProfessors() {
        return professorService.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Professor> findProfessorById(@PathVariable String id) {
        return this.professorService.findById(id)
                .map(professor -> ResponseEntity.ok().body(professor))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Professor> deleteProfessor(@PathVariable String id) {
        this.professorService.delete(id);
        if (this.professorService.findById(id).isEmpty())
            return ResponseEntity.ok().build();
        return ResponseEntity.badRequest().build();
    }

    @PostMapping("/add")
    public ResponseEntity<Professor> addProfessor(@RequestBody ProfessorDto professorDto) {
        return this.professorService.addProfessor(professorDto)
                .map(professor -> ResponseEntity.ok().body(professor))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PutMapping("/edit/{id}")
    public ResponseEntity<Professor> editProfessor(@PathVariable String id, @RequestBody ProfessorDto professorDto) {
        return this.professorService.editProfessor(id, professorDto)
                .map(professor -> ResponseEntity.ok().body(professor))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping("/byName")
    public List<Professor> findProfessorsByName(@RequestParam String name) {
        return professorService.findByName(name);
    }
}
