package mk.ukim.finki.wp.projects.web.rest;

import mk.ukim.finki.wp.projects.model.ScientificProjectCall;
import mk.ukim.finki.wp.projects.model.dto.AddScientificProjectCallDto;
import mk.ukim.finki.wp.projects.model.dto.ScientificProjectCallDto;
import mk.ukim.finki.wp.projects.service.ScientificProjectCallService;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/projects/scientificProjectCall")
@CrossOrigin(origins = {"http://localhost:3000","*"})
public class ScientificProjectCallController {
    private final ScientificProjectCallService scientificProjectCallService;

    public ScientificProjectCallController(ScientificProjectCallService scientificProjectCallService) {
        this.scientificProjectCallService = scientificProjectCallService;
    }

    @GetMapping("/all")
    public List<ScientificProjectCall> findAllCalls() {
        return this.scientificProjectCallService.findAll();
    }

    @GetMapping("/pagination")
    public List<ScientificProjectCall> findAllWithPagination(Pageable pageable)
    {
        return this.scientificProjectCallService.findAllByPagination(pageable).getContent();
    }


    @GetMapping("/{id}")
    public ResponseEntity<ScientificProjectCall> findCallById(@PathVariable Long id) {
        return this.scientificProjectCallService.findById(id)
                .map(call -> ResponseEntity.ok().body(call))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<ScientificProjectCall> deleteCall(@PathVariable Long id) {
        this.scientificProjectCallService.delete(id);
        if (this.scientificProjectCallService.findById(id).isEmpty())
            return ResponseEntity.ok().build();
        return ResponseEntity.badRequest().build();
    }

    @PostMapping("/add")
    public ResponseEntity<ScientificProjectCall> addCall(@RequestBody AddScientificProjectCallDto scientificProjectCallDto) {
        return this.scientificProjectCallService.addCall(scientificProjectCallDto)
                .map(call -> ResponseEntity.ok().body(call))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PutMapping("/edit/{id}")
    public ResponseEntity<ScientificProjectCall> editCall(@PathVariable Long id, @RequestBody ScientificProjectCallDto scientificProjectCallDto) {
        return this.scientificProjectCallService.editCall(id, scientificProjectCallDto)
                .map(call -> ResponseEntity.ok().body(call))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }
}
