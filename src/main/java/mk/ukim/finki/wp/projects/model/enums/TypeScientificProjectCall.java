package mk.ukim.finki.wp.projects.model.enums;

public enum TypeScientificProjectCall {
    OPENED,
    CLOSED
}
