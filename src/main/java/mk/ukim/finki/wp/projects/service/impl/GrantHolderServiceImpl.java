package mk.ukim.finki.wp.projects.service.impl;

import mk.ukim.finki.wp.projects.model.GrantHolder;
import mk.ukim.finki.wp.projects.model.dto.GrantHolderDto;
import mk.ukim.finki.wp.projects.model.exceptions.GrantHolderNotFound;
import mk.ukim.finki.wp.projects.repository.GrantHolderRepository;
import mk.ukim.finki.wp.projects.service.GrantHolderService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GrantHolderServiceImpl implements GrantHolderService {
    private final GrantHolderRepository grantHolderRepository;

    public GrantHolderServiceImpl(GrantHolderRepository grantHolderRepository) {
        this.grantHolderRepository = grantHolderRepository;
    }

    @Override
    public List<GrantHolder> findAll() {
        return grantHolderRepository.findAll();
    }

    @Override
    public Optional<GrantHolder> findById(Long id) {
        return grantHolderRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        GrantHolder grantHolder = this.findById(id).orElseThrow(GrantHolderNotFound::new);
        this.grantHolderRepository.delete(grantHolder);
    }

    @Override
    public Optional<GrantHolder> addInstitution(GrantHolderDto grantHolderDto) {
        GrantHolder grantHolder = new GrantHolder(grantHolderDto.getName(), grantHolderDto.getDescription());
        return Optional.of(this.grantHolderRepository.save(grantHolder));
    }

    @Override
    public Optional<GrantHolder> editInstitution(Long id, GrantHolderDto grantHolderDto) {
        GrantHolder grantHolder = this.findById(id).orElseThrow(GrantHolderNotFound::new);
        grantHolder.setName(grantHolderDto.getName());
        grantHolder.setDescription(grantHolderDto.getDescription());
        return Optional.of(this.grantHolderRepository.save(grantHolder));
    }

    @Override
    public List<GrantHolder> findByLocation(String description) {
        return grantHolderRepository.findByDescription(description);
    }

    @Override
    public GrantHolder findByName(String name) {
        return grantHolderRepository.findByName(name);
    }

}
