package mk.ukim.finki.wp.projects.service;

import mk.ukim.finki.wp.projects.model.Professor;
import mk.ukim.finki.wp.projects.model.dto.ProfessorDto;

import java.util.List;
import java.util.Optional;

public interface ProfessorService {
    List<Professor> findAll();

    Optional<Professor> findById(String id);

    void delete(String id);

    Optional<Professor> addProfessor(ProfessorDto professorDto);

    Optional<Professor> editProfessor(String id, ProfessorDto professorDto);

    List<Professor> findByName(String name);
}
