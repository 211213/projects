package mk.ukim.finki.wp.projects.model.dto;

import lombok.Data;
import mk.ukim.finki.wp.projects.model.enums.ProfessorTitle;

@Data
public class ProfessorDto {

    String id;
    String name;
    String email;
    ProfessorTitle title;

    public ProfessorDto(String name, String surname, String email, ProfessorTitle title) {
        this.name = name;
        this.email = email;
        this.title = title;
    }
}