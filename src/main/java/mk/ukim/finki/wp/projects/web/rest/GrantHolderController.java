package mk.ukim.finki.wp.projects.web.rest;

import mk.ukim.finki.wp.projects.model.GrantHolder;
import mk.ukim.finki.wp.projects.model.dto.GrantHolderDto;
import mk.ukim.finki.wp.projects.service.GrantHolderService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/projects/grantHolder")
@CrossOrigin(origins = {"http://localhost:3000","*"})
public class GrantHolderController {
    private final GrantHolderService grantHolderService;

    public GrantHolderController(GrantHolderService grantHolderService) {
        this.grantHolderService = grantHolderService;
    }

    @GetMapping("/all")
    public List<GrantHolder> findAllInstitutions() {
        return grantHolderService.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<GrantHolder> findInstitutionById(@PathVariable Long id) {
        return this.grantHolderService.findById(id)
                .map(institution -> ResponseEntity.ok().body(institution))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping("/add")
    public ResponseEntity<GrantHolder> addInstitution(@RequestBody GrantHolderDto grantHolderDto) {
        return this.grantHolderService.addInstitution(grantHolderDto)
                .map(institution -> ResponseEntity.ok().body(institution))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PutMapping("/edit/{id}")
    public ResponseEntity<GrantHolder> editInstitution(@PathVariable Long id, @RequestBody GrantHolderDto grantHolderDto) {
        return this.grantHolderService.editInstitution(id, grantHolderDto)
                .map(institution -> ResponseEntity.ok().body(institution))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<GrantHolder> deleteInstitution(@PathVariable Long id) {
        this.grantHolderService.delete(id);
        if (this.grantHolderService.findById(id).isEmpty())
            return ResponseEntity.ok().build();
        return ResponseEntity.badRequest().build();
    }

    @PostMapping("/byLocation")
    public List<GrantHolder> getAllByLocation(@RequestParam String location) {
        return grantHolderService.findByLocation(location);
    }

    @PostMapping("/byName")
    public GrantHolder findByName(@RequestParam String name) {
        return grantHolderService.findByName(name);
    }
}
