package mk.ukim.finki.wp.projects.service.impl;

import mk.ukim.finki.wp.projects.model.Professor;
import mk.ukim.finki.wp.projects.model.dto.ProfessorDto;
import mk.ukim.finki.wp.projects.model.exceptions.ProfessorNotFoundException;
import mk.ukim.finki.wp.projects.repository.ProfessorRepository;
import mk.ukim.finki.wp.projects.service.ProfessorService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProfessorServiceImpl implements ProfessorService {
    private final ProfessorRepository professorRepository;

    public ProfessorServiceImpl(ProfessorRepository professorRepository) {
        this.professorRepository = professorRepository;
    }

    @Override
    public List<Professor> findAll() {
        return professorRepository.findAll();
    }

    @Override
    public Optional<Professor> findById(String id) {
        return professorRepository.findById(id);
    }

    @Override
    public void delete(String id) {
        Professor professor = this.findById(id).orElseThrow(ProfessorNotFoundException::new);
        this.professorRepository.delete(professor);
    }

    @Override
    public Optional<Professor> addProfessor(ProfessorDto professorDto) {
        Professor professor = new Professor(professorDto.getId(), professorDto.getName(), professorDto.getEmail(), professorDto.getTitle());
        return Optional.of(this.professorRepository.save(professor));
    }

    @Override
    public Optional<Professor> editProfessor(String id, ProfessorDto professorDto) {
        Professor professor = this.findById(id).orElseThrow(ProfessorNotFoundException::new);
        professor.setName(professorDto.getName());
        professor.setEmail(professorDto.getEmail());
        professor.setTitle(professorDto.getTitle());
        return Optional.of(this.professorRepository.save(professor));
    }

    @Override
    public List<Professor> findByName(String name) {
        return professorRepository.findAllByName(name);
    }
}