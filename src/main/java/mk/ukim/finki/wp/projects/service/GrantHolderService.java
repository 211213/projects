package mk.ukim.finki.wp.projects.service;


import mk.ukim.finki.wp.projects.model.GrantHolder;
import mk.ukim.finki.wp.projects.model.dto.GrantHolderDto;

import java.util.List;
import java.util.Optional;

public interface GrantHolderService {
    List<GrantHolder> findAll();

    Optional<GrantHolder> findById(Long id);

    void delete(Long id);

    Optional<GrantHolder> addInstitution(GrantHolderDto grantHolderDto);


    Optional<GrantHolder> editInstitution(Long id, GrantHolderDto grantHolderDto);

    List<GrantHolder> findByLocation(String location);

    GrantHolder findByName(String name);

}
