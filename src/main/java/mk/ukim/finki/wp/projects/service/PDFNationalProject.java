package mk.ukim.finki.wp.projects.service;

import com.lowagie.text.DocumentException;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

public interface PDFNationalProject {
    public void exportNationalProject(HttpServletResponse response, Long nationalProjectId) throws DocumentException, IOException;
    public void nationalProjectReport(HttpServletResponse response) throws DocumentException, IOException;
}
