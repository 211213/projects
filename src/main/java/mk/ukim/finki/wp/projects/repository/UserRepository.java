package mk.ukim.finki.wp.projects.repository;

import mk.ukim.finki.wp.projects.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

}
