package mk.ukim.finki.wp.projects.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import mk.ukim.finki.wp.projects.model.enums.ScientificCallStatus;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class ScientificProjectCall {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private LocalDateTime createdAt;

    private LocalDateTime applicationDeadline;

    @ManyToOne
    ScientificProjectProgramme programme;


    @Enumerated(EnumType.STRING)
    private ScientificCallStatus status;

    public ScientificProjectCall(String name, LocalDateTime createdAt,
                                 LocalDateTime applicationDeadline,
                                 ScientificProjectProgramme programme, ScientificCallStatus status) {
        this.name = name;
        this.createdAt = createdAt;
        this.applicationDeadline = applicationDeadline;
        this.programme = programme;
        this.status = status;
    }
}
