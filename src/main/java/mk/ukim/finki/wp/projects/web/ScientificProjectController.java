package mk.ukim.finki.wp.projects.web;

import mk.ukim.finki.wp.projects.model.ScientificProject;
import mk.ukim.finki.wp.projects.model.enums.ScientificProjectStatus;
import mk.ukim.finki.wp.projects.service.ScientificProjectService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/scientific-project")
public class ScientificProjectController {

    private final ScientificProjectService scientificProjectService;

    public ScientificProjectController(ScientificProjectService scientificProjectService) {
        this.scientificProjectService = scientificProjectService;
    }

    @GetMapping({"/"})
    public String showScientificProjects (Model model,
                                          @RequestParam(defaultValue = "") String programmeName,
                                          @RequestParam(defaultValue = "") String grantHolderName,
                                          @RequestParam(defaultValue = "false") Boolean international,
                                          @RequestParam(defaultValue = "") ScientificProjectStatus status,
                                          @RequestParam(defaultValue = "") String name,
                                          @RequestParam(defaultValue = "") String professorName,
                                          @RequestParam(defaultValue = "") String scientificProjectCallName,
                                          @RequestParam(defaultValue = "1") Integer pageNum,
                                          @RequestParam(defaultValue = "20") Integer results){
        Page<ScientificProject> page = scientificProjectService.filterAndPaginateJoinedScientificProjects(programmeName, grantHolderName, international, status,
                                            name, professorName, scientificProjectCallName, pageNum, results);
        model.addAttribute("page", page);
        model.addAttribute("projects", this.scientificProjectService.findAll());

        return "projects/list.html";
    }

    @GetMapping("/add")
    public String showScientificProjectsForm (Model model){
        model.addAttribute("project", new ScientificProject());

        return null;
    }
}
