package mk.ukim.finki.wp.projects.repository;

import mk.ukim.finki.wp.projects.model.ScientificProjectProgramme;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScientificProjectProgrammeRepository extends JpaRepository<ScientificProjectProgramme, Long> {

}
