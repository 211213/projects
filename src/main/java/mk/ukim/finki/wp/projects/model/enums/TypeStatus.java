package mk.ukim.finki.wp.projects.model.enums;

public enum TypeStatus {
    OLD,
    NEW
}
