package mk.ukim.finki.wp.projects.model.dto;

import lombok.Data;
import mk.ukim.finki.wp.projects.model.enums.TypeScientificProjectCall;
import mk.ukim.finki.wp.projects.model.enums.TypeStatus;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
public class ScientificProjectCallDto {

    String name;
    String acronym;
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    LocalDate endDate;
    TypeScientificProjectCall typeScientificProjectCall;
    Long institutionId;
    TypeStatus typeStatus;

    public ScientificProjectCallDto(String name, String acronym, LocalDate endDate, TypeScientificProjectCall typeScientificProjectCall, Long institutionId, TypeStatus typeStatus) {
        this.name = name;
        this.acronym = acronym;
        this.endDate = endDate;
        this.typeScientificProjectCall = typeScientificProjectCall;
        this.institutionId = institutionId;
        this.typeStatus = typeStatus;
    }
}
