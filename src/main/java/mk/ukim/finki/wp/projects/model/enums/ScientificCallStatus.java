package mk.ukim.finki.wp.projects.model.enums;

public enum ScientificCallStatus {

    DRAFT, APPLICATION_PHASE, ACTIVE, REVIEW_FINISHED_PROJECTS, FINISHED
}
